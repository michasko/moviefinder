'use strict';

const config = require('./server/config/server.json');
const Server = require('./server/Server');

const server = new Server(Object.assign({}, config));

server.start();
