# MovieFinder

Simple app which allows browsing records in TheMovieDB.org database.

**Tested on NodeJS 8 / NPM 5 / Chrome 60**

## What's changed after feedback?
* Added tests for the front-end components
* Moved MovieDB service to the front-end. **Backend still acts as a proxy for that service, so that we don't expose our API key publicly**. Also, it allows implementing cache layer on our side, to limit number of requests send by our application to TheMovieDB.org API.
* Added TSLint for TypeScript (front-end) and ESLint for JavaScript (back-end) - **no CSS linter was added, as I couldn't find a reliable package**
* Moved search input into separate component with it's own logic
* Added routing
* Added documentation for front-end part of application as well

*INFO: Ammended some history because I found that I accidentaly added the API key in one of the commits :(*

## How it's done
* ReactJS + TypeScript on the front-end
* ExpressJS on the back-end (serves the application & proxies traffic to TheMovieDB.org API)
* Karma, Jasmine & Enzyme used for front-end testing
* Webpack is used for bundling front-end components

## How to launch
* Clone this repository: `git clone ssh://git@bitbucket.org/michasko/moviefinder.git`
* Go to cloned directory: `cd moviefinder`
* Paste the TheMovieDB.org API key (sent in an e-mail) in `server/config/moviedb.json` file
* Run `npm install && npm run build && npm start`
* Visit `http://localhost:1337/`
 
## How to run the tests
* Run `npm run test`

## How to run the TypeScript linter
* Run `npm run lint`

## TODO
* Provide custom styles for different device sizes
* Implement proper error responses for API endpoint
* Implement Cache service to limit the amount of requests sent to TheMovieDB.org API
