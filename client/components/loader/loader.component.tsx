import * as React from 'react';

import * as styles from './loader.component.styl';

export class Loader extends React.Component<{}, {}> {
	constructor() {
		super();
	}

	/**
	 * Renders the component
	 * @returns {JSX.Element}
	 */
	render(): JSX.Element {
		return (
			<div className={styles.bubbles}>
				<span className={styles.bubble} id='bubble1'></span>
				<span className={styles.bubble} id='bubble2'></span>
				<span className={styles.bubble} id='bubble3'></span>
			</div>
		);
	}
}
