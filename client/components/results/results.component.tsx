import * as React from 'react';
import { ResultItem, IResultItemProps } from './result-item.component';
import * as styles from './results.component.styl';

export interface IResultsProps {
	items: IResultItemProps[];
}

export class Results extends React.Component<IResultsProps, {}> {
	props: IResultsProps;

	constructor(props: IResultsProps) {
		super(props);
	}

	render(): JSX.Element {
		return (
			<ul className={styles.results}>
				{
					this.props.items.map((item) => {
						return <ResultItem key={item.key} {...item} />;
					})
				}
			</ul>
		);
	}
}
