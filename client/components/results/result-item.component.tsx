import * as React from 'react';
import * as styles from './result-item.component.styl';

export interface IResultItemProps {
	key: number;
	label: string;
	photoUrl: string;
	detailsUrl: string;
	date: string;
	description: string;
}

export class ResultItem extends React.Component<IResultItemProps, {}> {
	props: IResultItemProps;

	constructor(props: IResultItemProps) {
		super(props);
	}

	render(): JSX.Element  {
		const date = this.props.date ? (<span> ({this.props.date})</span>) : null;

		return (
			<li className={styles.resultItem}>
				<div>
					<img className={styles.image} src={this.props.photoUrl} alt={`${this.props.label} photo`}/>
					<a className={styles.title} href={this.props.detailsUrl} target='_blank'>{this.props.label}</a>{date}
					<p className={styles.description}>{this.props.description}</p>
				</div>
			</li>
		);
	}
}
