import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as jasmineEnzyme from 'jasmine-enzyme';

import { SearchBox } from './search-box.component';
import { ISearchBoxProps } from './search-box.component';

describe('SearchBox', () => {
	let props: ISearchBoxProps;
	let mountedSearchBox: ReactWrapper;

	const searchBox = (customProps?): ReactWrapper => {
		if (!mountedSearchBox) {
			mountedSearchBox = mount(
				<SearchBox {...Object.assign({}, props, customProps)} />
			);
		}
		return mountedSearchBox;
	};

	beforeEach(() => {
		jasmineEnzyme();

		props = {
			autoFocus: false,
			onSearch: jasmine.createSpy('onSearch'),
			initialValue: 'Dummy text',
			placeholder: ''
		};
		mountedSearchBox = void(0);
	});

	it('always renders a div', () => {
		const divs = searchBox().find('div');
		expect(divs.length).toBeGreaterThan(0);
	});

	it('should only fire a callback if searchTerm is not empty', () => {
		const instance: SearchBox = searchBox().instance() as SearchBox;

		instance.fireCallback();
		expect(instance.props.onSearch).not.toHaveBeenCalled();

		instance.setState({
			searchTerm: 'DummySearchTerm'
		});

		instance.fireCallback();
		expect(instance.props.onSearch).toHaveBeenCalledWith('DummySearchTerm');
	});

	it('class names for input should differ depending on whether input is empty or not', () => {
		const instance: SearchBox = searchBox().instance() as SearchBox;

		expect(instance.getInputClassNames()).not.toContain('with-value');

		instance.setState({
			searchTerm: 'DummyTerm'
		});

		expect(instance.getInputClassNames()).toContain('with-value');
	});

	it('should automatically focus input field if autoFocus prop is set to true', () => {
		const instance: SearchBox = searchBox({
			autoFocus: true
		}).instance() as SearchBox;

		spyOn(instance.input, 'focus');

		instance.componentDidMount();

		expect(instance.input.focus).toHaveBeenCalled();
	});

	it('should set placeholder for input', () => {
		const instance: SearchBox = searchBox({
			placeholder: 'Dummy'
		}).instance() as SearchBox;

		expect(instance.input.getAttribute('placeholder')).toEqual('Dummy');
	});
});
