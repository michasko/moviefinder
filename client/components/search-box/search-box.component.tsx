import * as React from 'react';
import * as styles from './search-box.component.styl';

export interface ISearchBoxProps {
	initialValue?: string;
	onSearch: (searchTerm: string) => any;
	autoFocus?: boolean;
	placeholder?: string;
}

export interface ISearchBoxState {
	searchTerm?: string;
}

export class SearchBox extends React.Component<ISearchBoxProps, ISearchBoxState> {
	props: ISearchBoxProps;
	state: ISearchBoxState = {};
	input: HTMLInputElement;

	static defaultProps: ISearchBoxProps = {
		onSearch: () => {},
		autoFocus: false
	};

	constructor(props: ISearchBoxProps) {
		super(props);
	}

	/**
	 * Component lifecycle hook. Set's up the component once it was mounted.
	 */
	componentDidMount(): void {
		if (this.props.autoFocus) {
			this.input.focus();

			// Move caret into right place
			if (this.props.initialValue) {
				this.input.selectionStart = this.props.initialValue.length;
				this.input.selectionEnd = this.props.initialValue.length;
			}
		}
	}

	/**
	 * Handles input events on input element
	 * @param searchTerm
	 */
	handleInput(searchTerm: string): void {
		this.setState({
			searchTerm
		});
	}

	/**
	 * Handles specific key presses, i.e. "Enter" key
	 * @param event
	 */
	handleKeyPress(event: KeyboardEvent): void {
		if (event.key === 'Enter') {
			this.fireCallback();
		}
	}

	/**
	 * Resets the state to initial values. Called after Click event on "Reset" button
	 */
	resetState(): void {
		this.input.value = '';
		this.setState({
			searchTerm: ''
		});
		this.input.focus();
	}

	/**
	 * Fires onSearch callback provided in props object with current search term as first argument
	 */
	fireCallback(): void {
		if (this.state.searchTerm) {
			this.props.onSearch(this.state.searchTerm);
		}
	}

	/**
	 * Returns a list of class names for input element
	 * @returns {string}
	 */
	getInputClassNames(): string {
		if (this.state.searchTerm) {
			return [ styles.searchBoxInput, styles.withValue ].join(' ');
		}

		return styles.searchBoxInput;
	}

	/**
	 * Renders the SearchBox component
	 * @returns {JSX.Element}
	 */
	render(): JSX.Element  {
		return (
			<div className={styles.searchBoxWrapper}>
				<input
						defaultValue={this.props.initialValue}
						onInput={(event: any) => this.handleInput(event.target.value)}
						onKeyPress={(event: any) => this.handleKeyPress(event)}
						className={this.getInputClassNames()}
						ref={ref => this.input = ref}
						type='text'
						placeholder={this.props.placeholder} />
				{this.state.searchTerm ? <span className={styles.searchBoxResetButton} onClick={() => this.resetState()} /> : null }
				<button type='button' className={styles.searchBoxSubmitButton} onClick={() => this.fireCallback()}>
					<i className='fa fa-search' aria-hidden='true'></i>
				</button>
			</div>
		);
	}
}
