export const searchBoxWrapper: string;
export const searchBoxInput: string;
export const withValue: string;
export const searchBoxResetButton: string;
export const searchBoxSubmitButton: string;
