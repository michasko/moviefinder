import { configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-15';

// Unfortunately there's no typing for the `__karma__` variable. Just declare it as any.
declare var __karma__: any;

// Prevent Karma from running prematurely.
__karma__.loaded = function (): void {};

// Find all the tests.
let context: any = (require as any).context('./', true, /\.spec\.tsx?/);

configure({
	adapter: new Adapter()
});

// And load the modules.
context.keys().map(context);
// Finally, start Karma to run the tests.
__karma__.start();
