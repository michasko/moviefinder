import { Person, Tv, Movie } from './moviedb.models';

export const MOVIE: Movie = {
	'media_type': 'movie',
	'vote_count': 2147,
	'id': 346364,
	'video': false,
	'vote_average': 7.4,
	'title': 'It',
	'popularity': 871.964102,
	'poster_path': '/9E2y5Q7WlCVNEhP5GiVTjhEhx1o.jpg',
	'original_language': 'en',
	'original_title': 'It',
	'genre_ids': [
		12,
		18,
		27
	],
	'backdrop_path': '/tcheoA2nPATCm2vvXw2hVQoaEFD.jpg',
	'adult': false,
	'overview': 'In a small town in Maine, seven children known as The Losers Club come face to face with life problems, bullies and a monster that takes the shape of a clown called Pennywise.',
	'release_date': '2017-09-05'
};

export const TV_SERIES: Tv = {
	'original_name': 'Breaking Bad',
	'id': 1396,
	'media_type': 'tv',
	'name': 'Breaking Bad',
	'vote_count': 1953,
	'vote_average': 8.2,
	'poster_path': '/1yeVJox3rjo2jBKrrihIMj7uoS9.jpg',
	'first_air_date': '2008-01-19',
	'popularity': 49.775803,
	'genre_ids': [
		18
	],
	'original_language': 'en',
	'backdrop_path': '/bzoZjhbpriBT2N5kwgK0weUfVOX.jpg',
	'overview': 'Breaking Bad is an American crime drama television series created and produced by Vince Gilligan. Set and produced in Albuquerque, New Mexico, Breaking Bad is the story of Walter White, a struggling high school chemistry teacher who is diagnosed with inoperable lung cancer at the beginning of the series. He turns to a life of crime, producing and selling methamphetamine, in order to secure his family\'s financial future before he dies, teaming with his former student, Jesse Pinkman. Heavily serialized, the series is known for positioning its characters in seemingly inextricable corners and has been labeled a contemporary western by its creator.',
	'origin_country': ['US']
};

export const PERSON: Person = {
	'popularity': 4.956529,
	'media_type': 'person',
	'id': 16483,
	'profile_path': '/gnmwOa46C2TP35N7ARSzboTdx2u.jpg',
	'name': 'Sylvester Stallone',
	'known_for': [
		{
			'vote_average': 6.9,
			'vote_count': 4495,
			'id': 607,
			'video': false,
			'media_type': 'movie',
			'title': 'Men in Black',
			'popularity': 15.943723,
			'poster_path': '/f24UVKq3UiQWLqGWdqjwkzgB8j8.jpg',
			'original_language': 'en',
			'original_title': 'Men in Black',
			'genre_ids': [
				28,
				12,
				35,
				878
			],
			'backdrop_path': '/uiZShvmW4rva88cSk800RLnGK01.jpg',
			'adult': false,
			'overview': 'Men in Black follows the exploits of agents Kay and Jay, members of a top-secret organization established to monitor and police alien activity on Earth. The two Men in Black find themselves in the middle of the deadly plot by an intergalactic terrorist who has arrived on Earth to assassinate two ambassadors from opposing galaxies. In order to prevent worlds from colliding, the MiB must track down the terrorist and prevent the destruction of Earth. It\'s just another typical day for the Men in Black.',
			'release_date': '1997-07-02'
		},
		{
			'vote_average': 7.6,
			'vote_count': 4877,
			'id': 283995,
			'video': false,
			'media_type': 'movie',
			'title': 'Guardians of the Galaxy Vol. 2',
			'popularity': 179.721862,
			'poster_path': '/y4MBh0EjBlMuOzv9axM4qJlmhzz.jpg',
			'original_language': 'en',
			'original_title': 'Guardians of the Galaxy Vol. 2',
			'genre_ids': [
				28,
				12,
				35,
				878
			],
			'backdrop_path': '/aJn9XeesqsrSLKcHfHP4u5985hn.jpg',
			'adult': false,
			'overview': 'The Guardians must fight to keep their newfound family together as they unravel the mysteries of Peter Quill\'s true parentage.',
			'release_date': '2017-04-19'
		},
		{
			'vote_average': 6,
			'vote_count': 2958,
			'id': 27578,
			'video': false,
			'media_type': 'movie',
			'title': 'The Expendables',
			'popularity': 20.383406,
			'poster_path': '/y2qJoYxOhzyidsA60Mqn29H38Lk.jpg',
			'original_language': 'en',
			'original_title': 'The Expendables',
			'genre_ids': [
				53,
				12,
				28
			],
			'backdrop_path': '/x5u73uBylbyCCnkzUGzt3uozqRp.jpg',
			'adult': false,
			'overview': 'Barney Ross leads a band of highly skilled mercenaries including knife enthusiast Lee Christmas, a martial arts expert, heavy weapons specialist, demolitionist, and a loose-cannon sniper. When the group is commissioned by the mysterious Mr. Church to assassinate the dictator of a small South American island, Barney and Lee visit the remote locale to scout out their opposition and discover the true nature of the conflict engulfing the city.',
			'release_date': '2010-08-03'
		}
	],
	'adult': false
};
