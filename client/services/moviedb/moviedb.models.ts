export interface Movie {
	media_type?: string;
	vote_count: number;
	id: number;
	video: boolean;
	vote_average: number;
	title: string;
	popularity: number;
	poster_path: string;
	original_language: string;
	original_title: string;
	genre_ids: number[];
	backdrop_path: string;
	adult: boolean;
	overview: string;
	release_date: string;
}

export interface Tv {
	media_type?: string;
	original_name: string;
	genre_ids: number[];
	name: string;
	popularity: number;
	origin_country: string[];
	vote_count: number;
	first_air_date: string;
	backdrop_path: string;
	original_language: string;
	id: number;
	vote_average: number;
	overview: string;
	poster_path: string;
}

export interface Person {
	media_type?: string;
	popularity: number;
	id: number;
	profile_path: string;
	name: string;
	known_for: Array<Movie | Tv>;
	adult: boolean;
}

export interface MovieDbResponse {
	page: number;
	total_results: number;
	total_pages: number;
	results: Array<Movie | Tv | Person>;
}

export type MovieDbItem = Movie | Tv | Person;

export function isMovie(item: MovieDbItem): item is Movie {
	return item.media_type === 'movie';
}

export function isTv(item: MovieDbItem): item is Tv {
	return item.media_type === 'tv';
}

export function isPerson(item: MovieDbItem): item is Person {
	return item.media_type === 'person';
}
