import { MovieDbService, IMAGES_BASE_URI } from './moviedb.service';
import { MOVIE, TV_SERIES, PERSON } from './moviedb.fixtures';
import { IResultItemProps } from '../../components/results/result-item.component';

describe('MovieDB Service', () => {
	it('should add "media_type" property for "popular" endpoint', () => {
		const popularItem: any = Object.assign({}, MOVIE);

		delete popularItem['media_type'];

		spyOn(window, 'fetch').and.returnValue(Promise.resolve({
			results: [ popularItem ]
		}));

		MovieDbService.getPopularMovies()
			.then((items: IResultItemProps[]) => {
				expect(items[0]).toEqual(MovieDbService.mapToResultItem(popularItem) as any);
			});
	});

	it('should not map media types it doesn\'t know', () => {
		const unknownItem: any = Object.assign({}, PERSON, { 'media_type': 'unknown' });

		expect(MovieDbService.mapToResultItem(unknownItem)).toBe(null);
	});

	it('should map media type "movie" to ResultItem', () => {
		expect(MovieDbService.mapToResultItem(MOVIE)).toEqual({
			key: MOVIE.id,
			label: MOVIE.title,
			photoUrl: `${IMAGES_BASE_URI}${MOVIE.poster_path}`,
			detailsUrl: `https://www.themoviedb.org/movie/${MOVIE.id}`,
			date: MOVIE.release_date,
			description: MOVIE.overview
		} as any);
	});

	it('should map media type "tv" to ResultItem', () => {
		expect(MovieDbService.mapToResultItem(TV_SERIES)).toEqual({
			key: TV_SERIES.id,
			label: TV_SERIES.original_name,
			photoUrl: `${IMAGES_BASE_URI}${TV_SERIES.poster_path}`,
			detailsUrl: `https://www.themoviedb.org/tv/${TV_SERIES.id}`,
			date: TV_SERIES.first_air_date,
			description: TV_SERIES.overview
		} as any);
	});

	it('should map media type "person" to ResultItem', () => {
		expect(MovieDbService.mapToResultItem(PERSON)).toEqual({
			key: PERSON.id,
			label: PERSON.name,
			photoUrl: `${IMAGES_BASE_URI}${PERSON.profile_path}`,
			detailsUrl: `https://www.themoviedb.org/person/${PERSON.id}`,
			date: void(0),
			description: 'Known for: Men in Black, Guardians of the Galaxy Vol. 2, The Expendables'
		} as any);
	});
});
