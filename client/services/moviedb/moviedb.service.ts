import { Movie, MovieDbResponse, MovieDbItem, isPerson, isTv, isMovie } from './moviedb.models';
import { IResultItemProps } from '../../components/results/result-item.component';

export const IMAGES_BASE_URI: string = 'http://image.tmdb.org/t/p/w185';
const MEMORY_CACHE: any = {};

export class MovieDbService {
	/**
	 * Gets 5 most popular movies at the time of invocation
	 * @returns {any}
	 */
	static getPopularMovies(): Promise<IResultItemProps[]> {
		const url: string = '/api/movie/popular';

		if (MEMORY_CACHE[url]) {
			return Promise.resolve(MEMORY_CACHE[url]);
		}

		return fetch(url)
			.then(res => res.json())
			.then((res: MovieDbResponse) => {
				return res.results.slice(0, 5);
			})
			.then((movies: Movie[]) => {
				return movies.map(movie => Object.assign({ media_type: 'movie' }, movie));
			})
			.then((movies: Movie[]) => {
				return movies.map(movie => MovieDbService.mapToResultItem(movie));
			})
			.then((items: IResultItemProps[]) => {
				MEMORY_CACHE[url] = items;

				return items;
			});
	}

	/**
	 * Performs a search in TheMovieDB database, for any media type (movie, tv series, people)
	 * @param query
	 * @returns {any}
	 */
	static search(query: string): Promise<IResultItemProps[]> {
		const url: string = `/api/search/multi?query=${query}`;

		if (MEMORY_CACHE[url]) {
			return Promise.resolve(MEMORY_CACHE[url]);
		}

		return fetch(url)
			.then(res => res.json())
			.then((res: MovieDbResponse) => res.results)
			.then((items: MovieDbItem[]) => {
				return items.map(item => MovieDbService.mapToResultItem(item));
			})
			.then((items: IResultItemProps[]) => {
				MEMORY_CACHE[url] = items;

				return items;
			});
	}

	/**
	 * Maps query result to IResultItemProps, for Results component usage.
	 * Returns null if doesn't know how to map particular result.
	 *
	 * @param item MovieDbItem
	 * @returns {IResultItemProps | null}
	 */
	static mapToResultItem(item: MovieDbItem): IResultItemProps | null {
		if (isPerson(item)) {
			return {
				key: item.id,
				label: item.name,
				photoUrl: MovieDbService.getPhotoUrl(item.profile_path),
				detailsUrl: `https://www.themoviedb.org/person/${item.id}`,
				date: void(0),
				description: MovieDbService.getKnownFor(item)
			};
		}
		else if (isMovie(item)) {
			return {
				key: item.id,
				label: item.title,
				photoUrl: MovieDbService.getPhotoUrl(item.poster_path),
				detailsUrl: `https://www.themoviedb.org/movie/${item.id}`,
				date: item.release_date,
				description: item.overview
			};
		}
		else if (isTv(item)) {
			return {
				key: item.id,
				label: item.name,
				photoUrl: MovieDbService.getPhotoUrl(item.poster_path),
				detailsUrl: `https://www.themoviedb.org/tv/${item.id}`,
				date: item.first_air_date,
				description: item.overview
			};
		}

		return null;
	}


	/**
	 * Returns description data for people (actors, directors, etc.)
	 * @param personData TheMovieDB.org API person-related data
	 * @returns {string}
	 */
	static getKnownFor(personData) {
		const titles = personData.known_for.slice(0, 5).map(media => media.title || media.name);

		return `Known for: ${titles.join(', ')}`;
	}

	/**
	 * Concatenates TheMovieDB.org images base URI & provided image path.
	 * If no path is provided, URL to dummy photo is returned.
	 * @param path
	 * @returns {*}
	 */
	static getPhotoUrl(path) {
		if (path) {
			return `${IMAGES_BASE_URI}${path}`;
		}

		return '/assets/no-photo.png';
	}
}
