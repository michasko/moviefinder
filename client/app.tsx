import * as ReactDOM from 'react-dom';
import * as React from 'react';
import { BrowserRouter, Link, withRouter } from 'react-router-dom';
import { SearchBox } from './components/search-box/search-box.component';
import { Main } from './containers/main.component';

const App = withRouter(({ history, match }) => (
	<div>
		<header>
			<h1><Link to='/'>MovieFinder</Link></h1>
			<SearchBox {...{
				onSearch: (searchTerm: string) => {
					history.push(`/search/${searchTerm}`);
				},
				autoFocus: true,
				initialValue: history.location.pathname.slice(history.location.pathname.lastIndexOf('/') + 1),
				placeholder: 'Type here to find movies, TV shows or people...'
			}} />
		</header>
		<Main/>
	</div>
));

ReactDOM.render(
	<BrowserRouter>
		<App/>
	</BrowserRouter>,
	document.querySelector('#root')
);
