import * as React from 'react';

import { Results} from '../../components/results/results.component';
import { IResultItemProps } from '../../components/results/result-item.component';
import { MovieDbService } from '../../services/moviedb/moviedb.service';
import { Loader } from '../../components/loader/loader.component';

interface IHomeState {
	items: IResultItemProps[];
	isLoading: boolean;
}

export class Home extends React.Component<{}, IHomeState> {
	state: IHomeState;

	constructor() {
		super();
	}

	/**
	 * Set's the initial state of the component
	 */
	componentWillMount(): void {
		this.setState({
			isLoading: true,
			items: []
		});
	}

	/**
	 * Method responsible for providing initial data using MovieDB Service
	 */
	componentDidMount(): void {
		MovieDbService
			.getPopularMovies()
			.then((movies: IResultItemProps[]) => this.setState({
				isLoading: false,
				items: movies
			}));
	}

	/**
	 * Renders the component
	 * @returns {JSX.Element}
	 */
	render(): JSX.Element {
		return (
			<section>
				<h2>Most popular movies</h2>
				{this.state.isLoading ? <Loader/> : null }
				<Results items={this.state.items} />
			</section>
		);
	}
}
