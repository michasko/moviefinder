import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as jasmineEnzyme from 'jasmine-enzyme';

import { Home } from './home.component';

describe('Home', () => {
	let mountedSearchBox: ReactWrapper;

	const home = (): ReactWrapper => {
		if (!mountedSearchBox) {
			mountedSearchBox = mount(
				<Home/>
			);
		}
		return mountedSearchBox;
	};

	beforeEach(() => {
		jasmineEnzyme();
		mountedSearchBox = void(0);
	});

	it('should have initial state with isLoading=true', () => {
		const instance: Home = home().instance() as Home;

		expect(instance.state.isLoading).toBeTruthy();
	});

	it('should render loader if isLoading flag is set to true', () => {
		const wrapper: ReactWrapper = home();

		wrapper.setState({
			isLoading: true
		});

		expect(wrapper.find('Loader').length).toBe(1);

		wrapper.setState({
			isLoading: false
		});

		expect(wrapper.find('Loader').length).toBe(0);
	});
});
