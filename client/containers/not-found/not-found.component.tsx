import * as React from 'react';
import { Link } from 'react-router-dom';

export class NotFound extends React.Component<{}, {}> {
	constructor() {
		super();
	}

	/**
	 * Renders the component
	 * @returns {JSX.Element}
	 */
	render(): JSX.Element {
		return (
			<section>
				<h2>Seems like you got lost my friend :(</h2>
				<Link to='/'>Return to homepage?</Link>
			</section>
		);
	}
}
