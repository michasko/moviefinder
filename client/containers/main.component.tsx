import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Home } from './home/home.component';
import { Search } from './search/search.component';
import { NotFound } from './not-found/not-found.component';

import * as styles from './main.component.styl';

export class Main extends React.Component<{}, {}> {
	constructor() {
		super();
	}

	/**
	 * Renders the component
	 * @returns {JSX.Element}
	 */
	render(): JSX.Element {
		return (
			<main className={styles.mainContent}>
				<Switch>
					<Route exact path='/' component={Home}/>
					<Route path='/search/:searchTerm' component={Search}/>
					<Route component={NotFound}/>
				</Switch>
			</main>
		);
	}
}
