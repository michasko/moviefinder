import * as React from 'react';

import { Loader } from '../../components/loader/loader.component';
import { Results} from '../../components/results/results.component';
import { IResultItemProps } from '../../components/results/result-item.component';
import { MovieDbService } from '../../services/moviedb/moviedb.service';

interface ISearchState {
	items: IResultItemProps[];
	isLoading: boolean;
}

export interface ISearchProps {
	match: {
		params: {
			searchTerm: string;
		}
	};
}

export class Search extends React.Component<ISearchProps, ISearchState> {
	state: ISearchState;
	props: ISearchProps;

	constructor() {
		super();
	}

	/**
	 * Set's the initial state of the component
	 */
	componentWillMount(): void {
		this.setState({
			isLoading: true,
			items: []
		});
	}

	/**
	 * Lifecycle hook which get's called when component's properties change.
	 * @param nextProps
	 */
	componentWillReceiveProps(nextProps: ISearchProps): void {
		if (this.props.match.params.searchTerm !== nextProps.match.params.searchTerm) {
			this.search(nextProps.match.params.searchTerm);
		}
	}

	/**
	 * Lifecycle hook which get's called upon the component mounting.
	 * Calls MovieDB API in case when user get's directly to /search/query route
	 */
	componentDidMount(): void {
		this.search(this.props.match.params.searchTerm);
	}

	/**
	 * Calls the MovieDB service's "search" method with provided searchTerm
	 * @param searchTerm
	 */
	search(searchTerm: string): void {
		MovieDbService
			.search(searchTerm)
			.then((items: IResultItemProps[]) => this.setState({
				isLoading: false,
				items
			}));
	}

	/**
	 * Flag used in component render, determines whether or not the items collection should be displayed.
	 * @returns {boolean}
	 */
	get showItems(): boolean {
		return !this.state.isLoading && this.state.items.length > 0;
	}

	/**
	 * Flag used in component render, determines whether or not the information about no results should be displayed.
	 * @returns {boolean}
	 */
	get showNoResultsMessage(): boolean {
		return !this.state.isLoading && this.state.items.length === 0;
	}

	/**
	 * Renders the component
	 * @returns {JSX.Element}
	 */
	render(): JSX.Element {
		return (
			<section>
				<h2>Search results for "{this.props.match.params.searchTerm}":</h2>
				{ this.state.isLoading ? <Loader/> : null }
				{ this.showItems ? <Results items={this.state.items} /> : null }
				{ this.showNoResultsMessage ? <p className='no-results'>No results, sorry :(</p> : null }
			</section>
		);
	}
}
