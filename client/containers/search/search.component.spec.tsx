import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as jasmineEnzyme from 'jasmine-enzyme';

import { Search } from './search.component';
import { ISearchProps } from './search.component';

describe('Search', () => {
	let props: ISearchProps;
	let mountedSearchBox: ReactWrapper;

	const search = (customProps?): ReactWrapper => {
		if (!mountedSearchBox) {
			mountedSearchBox = mount(
				<Search {...Object.assign({}, props, customProps)} />
			);
		}
		return mountedSearchBox;
	};

	beforeEach(() => {
		jasmineEnzyme();

		props = {
			match: {
				params: {
					searchTerm: ''
				}
			}
		};
		mountedSearchBox = void(0);
	});

	it('always renders a section element', () => {
		expect(search().find('section').length).toBeGreaterThan(0);
	});

	it('should have initial state with isLoading=true', () => {
		const instance: Search = search().instance() as Search;

		expect(instance.state.isLoading).toBeTruthy();
	});

	it('should render loader if isLoading flag is set to true', () => {
		const wrapper: ReactWrapper = search();

		wrapper.setState({
			isLoading: true
		});

		expect(wrapper.find('Loader').length).toBe(1);

		wrapper.setState({
			isLoading: false
		});

		expect(wrapper.find('Loader').length).toBe(0);
	});

	it('should show items if data is loaded and we have at least one result', () => {
		const wrapper: ReactWrapper = search();
		const instance: Search = wrapper.instance() as Search;

		wrapper.setState({
			isLoading: false,
			items: [{}]
		});

		expect(instance.showItems).toBe(true);
		expect(wrapper.find('Results').length).toBe(1);
		expect(wrapper.find('.no-results').length).toBe(0);

		wrapper.setState({
			isLoading: false,
			items: []
		});

		expect(instance.showItems).toBe(false);
		expect(wrapper.find('Results').length).toBe(0);
		expect(wrapper.find('.no-results').length).toBe(1);
	});
});
