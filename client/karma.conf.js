// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

const webpack = require('webpack');

module.exports = function (config) {
	const testWebpackConfig = require('../webpack.config');

	config.set({

		basePath: '',

		mime: { 'text/x-typescript': ['ts','tsx'] },

		frameworks: ['jasmine'],
		files: [
			{ pattern: './test.ts', watched: false }
		],

		preprocessors: {
			'./test.ts': ['webpack', 'sourcemap']
		},

		remapIstanbulReporter: {
			reports: {
				html: 'coverage',
				lcovonly: './coverage/coverage.lcov'
			}
		},

		webpack: Object.assign({}, testWebpackConfig, { devtool: 'inline-source-map' }),

		coverageReporter: {
			type: 'in-memory'
		},

		remapCoverageReporter: {
			'text-summary': null,
			json: './coverage/coverage.json',
			html: './coverage/html'
		},


		// Webpack please don't spam the console when running in karma!
		webpackMiddleware: {
			// webpack-dev-middleware configuration
			// i.e.
			noInfo: true,
			// and use stats to turn off verbose output
			stats: {
				// options i.e.
				chunks: false
			}
		},

		port: 9876,

		colors: true,

		/*
		 * level of logging
		 * possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		 */
		logLevel: config.LOG_INFO,

		autoWatch: false,

		browsers: ['Chrome'],

		singleRun: true
	});
};
