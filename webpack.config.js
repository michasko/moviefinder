const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { resolve } = require('path');

module.exports = {
	entry: {
		app: './client/app.tsx'
	},
	output: {
		filename: 'app.js',
		path: resolve(__dirname, 'public'),
		publicPath: '/'
	},
	devtool: 'source-map',
	resolve: {
		extensions: ['.js', '.ts', '.tsx', '.styl']
	},
	module: {
		rules: [
			{
				test: /\.styl$/,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							modules: true,
							importLoaders: 1,
							camelCase: true,
							localIdentName: '[name]__[local]___[hash:base64:5]',
							url: false
						}
					},
					{
						loader: 'typed-css-modules-loader',
						options: {
							camelCase: true
						}
					},
					{
						loader: 'stylus-loader',
						options: {
							sourceMap: false,
							paths: ['client']
						}
					}
				]
			},
			{
				test: /\.tsx?$/,
				use: [
					'awesome-typescript-loader'
				],
				exclude: [
					/node_modules/
				]
			},
			{
				test: /\.twig$/,
				loader: 'twig-loader'
			},
		]
	},
	plugins: [
		new CopyWebpackPlugin([
			{
				context: 'client',
				from: 'assets/**'
			}
		]),
		new HtmlWebpackPlugin({
			template: 'client/index.html.twig',
			inject: 'body',
			chunks: ['app']
		})
	]
};
