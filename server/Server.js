'use strict';

const path = require('path');
const Express = require('express');
const proxy = require('express-http-proxy');

const movieDbConfig = require('./config/moviedb.json');

class Server {
	/**
	 * @constructor
	 * @param config
	 */
	constructor(config) {
		this.config = Object.assign({}, config);
		this.app = Express();

		this.configure();
	}

	/**
	 * Start to listen on configured host & port.
	 * This method should not be called by the instance itself to enable easier testing.
	 */
	start() {
		const server = this.app.listen(this.config.port, this.config.hostname, () => {
			const connection = server.address();

			console.info(`Setup & listening at ${connection.address}:${connection.port}`); // eslint-disable-line
		});
	}

	/**
	 * Configure endpoints & middleware handled by the server
	 */
	configure() {
		// Serving assets
		this.app.use(Express.static(path.join(process.cwd(), this.config.staticDir)));

		// Let ExpressJS act as a proxy to TheMovieDB, so that we don't expose our API key.
		this.app.use('/api', proxy(movieDbConfig.baseUri, {
			proxyReqPathResolver: (req) => {
				const keyPrefix = req.url.indexOf('?') < 0 ? '?' : '&';

				return`/${movieDbConfig.version}${req.url}${keyPrefix}api_key=${movieDbConfig.apiKey}`;
			}
		}));

		this.app.get('*', (req, response) => {
			response.sendFile(path.join(process.cwd(), this.config.staticDir, 'index.html'));
		});
	}
}

module.exports = Server;
